from rest_framework import serializers
from . import models

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CustomUser
        fields = ('email', 'password', 'username', 'name', 'firebase_uid', 'firebase_auth')

    def create(self, validated_data):
        return models.CustomUser.objects.create(**validated_data)



