# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser, User
from datetime import datetime

# Create your models here.

class CustomUser(AbstractUser):
    name = models.CharField(blank=True, max_length=255)
    firebase_auth = models.TextField(null=True, blank=True, max_length=255)
    firebase_uid = models.TextField(null=True, blank=True, max_length=255)

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        return super(CustomUser, self).save(*args, **kwargs)

class Setting(models.Model):
    api_key = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.api_key


class Log_Category(models.Model):
    name = models.TextField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    def soft_delete(self):
        self.deleted_at = datetime.now()
        self.save()

class Log(models.Model):
    log = models.TextField(max_length=255)
    category = models.ForeignKey(
        Log_Category, related_name='logs', on_delete=models.CASCADE,
        blank=True, null=True)
    user = models.ForeignKey(CustomUser, related_name='logs',
                             on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.log

