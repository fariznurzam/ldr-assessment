from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views
from django.views.decorators.csrf import csrf_exempt

app_name = "dashboard"
urlpatterns = [
    path('users/', views.UserListView.as_view(), name='users'),
    path('users/create/', csrf_exempt(views.UserCreateView.as_view()), name='users_create'),
    path('users/<int:pk>/', views.UserDetailView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
