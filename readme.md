LDR Assessment
======

How to run the app
------
1. Clone the project
2. `source venv/bin/activate`
3. `cd mainproject`
4. Make sure your local has MongoDB
5. `python manage.py migrate`
6. `python manage.py runserver`

![alt text](https://thumb.ibb.co/nK6Lq8/Architecture.png "Architecture Diagram")
![alt text](https://thumb.ibb.co/nfgaOT/Class_Diagram.png "Class Diagram")
